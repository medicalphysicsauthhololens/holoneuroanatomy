﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class AnatomyManager : MonoBehaviour
{

    public static AnatomyManager Instance { get; private set; }

    public GameObject gameObjectHumanBody;
    public GameObject gameObjectHumanBodyTransparent;
    public GameObject gameObjectBrainWithStelexos;
    public GameObject gameObjectMedula;
    public GameObject gameObjectStart;
    public GameObject gameObjectFilmViewMessage;
    public GameObject gameObjectLabel1;
    public GameObject gameObjectLabel2;
    public GameObject gameObjectLabel3;
    public GameObject gameObjectLabel4;
    public GameObject gameObjectLabel5;
    public GameObject gameObjectLabel6;
    public GameObject gameObjectLabel7;
    public GameObject gameObjectLabel8;
    public GameObject gameObjectLabel9;
    public GameObject gameObjectLabel10;
    public GameObject gameObjectLabel11;
    public GameObject gameObjectLabel12;
    public GameObject gameObjectLabel13;
    public GameObject gameObjectLabel14;
    public GameObject gameObjectLabel15;
    public GameObject gameObjectLabel16;
    public GameObject gameObjectLabel17;
    public GameObject gameObjectLabel18;

    public AudioSource audioSourceAnswer;
    public AudioClip audioAnswer;

    public enum Stages { Start, Stage1, Stage2, Stage3, Stage4, Stage5, End }
    public string stage;
    private float transparencyValue = 1.0f;
    void Start()
    {
        stage = Stages.Start.ToString();
        gameObjectHumanBodyTransparent.GetComponent<MeshRenderer>().sharedMaterial.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        gameObjectBrainWithStelexos.SetActive(false);
        gameObjectLabel1.SetActive(false);
        gameObjectLabel2.SetActive(false);
        gameObjectLabel3.SetActive(false);
        gameObjectLabel4.SetActive(false);
        gameObjectLabel5.SetActive(false);
        gameObjectLabel6.SetActive(false);
        gameObjectLabel7.SetActive(false);
        gameObjectLabel8.SetActive(false);
        gameObjectLabel9.SetActive(false);
        gameObjectLabel10.SetActive(false);
        gameObjectLabel11.SetActive(false);
        gameObjectLabel12.SetActive(false);
        gameObjectLabel13.SetActive(false);
        gameObjectLabel14.SetActive(false);
        gameObjectLabel15.SetActive(false);
        gameObjectLabel16.SetActive(false);
        gameObjectLabel17.SetActive(false);
        gameObjectLabel18.SetActive(false);
        gameObjectMedula.SetActive(false);

        //OnSelectStart();
    }

    void Awake()
    {
        Instance = this;
    }

    void Update()
    {

        if (stage == Stages.Stage1.ToString())
        {
            if (transparencyValue > 0.1f)
            {
                gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("A young man visits his GP with the following pattern of sensory loss:{0}Loss of pain, temperature, touch and pressure on the ipsilateral side of the face.{0}Loss of pain, temperature, touch and pressure on the contralateral side of the entire body{0}Explain the above findings on the basis of a single lesion involving the CNS. Estimate the approximate location a lesion could be located.{0}Where would it be more possible for the causing lesion to be located?", Environment.NewLine), 52);
                transparencyValue = transparencyValue - 0.01f;
                gameObjectHumanBodyTransparent.GetComponent<MeshRenderer>().sharedMaterial.color = new Color(1.0f, 1.0f, 1.0f, transparencyValue);
                gameObjectLabel2.SetActive(true);
            }
            else
            {
                gameObjectHumanBody.SetActive(false);
                gameObjectLabel1.SetActive(true);
                gameObjectLabel3.SetActive(true);
                gameObjectLabel4.SetActive(true);
            }
        }

    }

    public void OnSelectStart()
    {
        gameObjectStart.SetActive(false);
        stage = Stages.Stage1.ToString();
        audioSourceAnswer.PlayOneShot(audioAnswer);
    }

    public void OnSelectBrain()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectLabel1.SetActive(false);
        gameObjectLabel2.SetActive(false);
        gameObjectLabel3.SetActive(false);
        gameObjectLabel4.SetActive(false);

        gameObjectLabel5.SetActive(true);
        gameObjectLabel6.SetActive(true);
        gameObjectLabel7.SetActive(true);
        gameObjectLabel8.SetActive(true);
        stage = Stages.Stage2.ToString();

        gameObjectBrainWithStelexos.SetActive(true);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("Where in the  brain?", Environment.NewLine), 52);
    }
    public void OnSelectSpinalCord()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectLabel2.SetActive(false);
    }
    public void OnSelectPeripheralNervousSystem()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("Such a clearly defined sensory loss (ipsilateral of the face, contralateral of the body) would not be expected from a diffuse peripheral neuropathy. Also, you would expect sensory modalities to be affected inconsistently or you would expect other sensory modalities to be occasionally affected as well.", Environment.NewLine), 52);

        //Fail
        RegenerateRoom();
    }
    public void OnSelectPatternOfSensoryLoss()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("Somebody just died from a PICA aneurysm or wallenberg syndrome.", Environment.NewLine), 52);

        //Fail
        RegenerateRoom();
    }

    
    public void OnSelectBrainstem()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectLabel5.SetActive(false);
        gameObjectLabel6.SetActive(false);
        gameObjectLabel7.SetActive(false);
        gameObjectLabel8.SetActive(false);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("So, we have narrowed it down to the brainstem. Which sensory cranial nerve are we looking for there ?", Environment.NewLine), 52);

        gameObjectLabel9.SetActive(true);
        gameObjectLabel10.SetActive(true);
        gameObjectLabel11.SetActive(true);
        gameObjectLabel12.SetActive(true);

        stage = Stages.Stage3.ToString();
    }
    public void OnSelectSomatosensoryCortex()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("The somatosensory cortex (post central gyrus) features a somatotopic representation of sensory input from the contralateral part of the body, limbs and face spanning the whole cortical strip. A lesion large enough to affect all those areas would probably affect surrounding structures as well (eg pre - central gyrus / motor strip).Moreover, sensory loss would be in the same side in both body and face(contralateral to the lesion)", Environment.NewLine), 52);

        gameObjectLabel5.SetActive(false);
    }
    public void OnSelectSubcorticalWhite()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("The 3rd sensory neuron bodies are located in the ventral posterior thalamus and their axons are congregated as they leave the thalamus to form the subcortical white matter tract. A small lesion here would probably affect both the face and body.But, sensory loss would be in the same side in both body and face(contralateral to the lesion)", Environment.NewLine), 52);

        gameObjectLabel6.SetActive(false);
    }
    public void OnSelectNotInBrain()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        //Fail
        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("Fail. Please try again.", Environment.NewLine), 52);

        RegenerateRoom();
    }


    public void OnSelectTrigeminal()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectLabel9.SetActive(false);
        gameObjectLabel10.SetActive(false);
        gameObjectLabel11.SetActive(false);
        gameObjectLabel12.SetActive(false);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("For the sensory loss to be in contralateral side of body and ispilateral side of face: {0}1) Spinothalamic fibers from the contralateral side of the body, having crossed at the spinal cord level continue inwards (up to the thalamus) at the side of the lesion.{0}trigeminal fibers from the ipsilateral side of face, carrying pain, temperature, touch and pressure input, cannot have already crossed to the other side.{0}Where in the  brainstem are those criteria met?", Environment.NewLine), 52);

        gameObjectLabel13.SetActive(true);
        gameObjectLabel14.SetActive(true);
        gameObjectLabel15.SetActive(true);
        gameObjectLabel16.SetActive(true);

        stage = Stages.Stage4.ToString();

    }
    public void OnSelectFacial()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("The facial nerve carries sensory information from the external ear.", Environment.NewLine), 52);

        gameObjectLabel10.SetActive(false);
    }
    public void OnSelectGlossohargeal()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("The glossopharyngeal nerve carries sensory information from the post 1/3 of tongue, pharynx and middel ear.", Environment.NewLine), 52);

        gameObjectLabel11.SetActive(false);
    }
    public void OnSelectVagus()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("The vagus nerve carries sensory information from auditory canal, larynx, pharynx and oesophagus.", Environment.NewLine), 52);

        gameObjectLabel12.SetActive(false);
    }



    public void OnSelectMedusa()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectLabel13.SetActive(false);
        gameObjectLabel14.SetActive(false);
        gameObjectLabel15.SetActive(false);
        gameObjectLabel16.SetActive(false);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("Can we predict where in the medula with the information we have so far?", Environment.NewLine), 52);

        gameObjectMedula.SetActive(true);
        gameObjectLabel17.SetActive(true);
        gameObjectLabel18.SetActive(true);

        stage = Stages.Stage5.ToString();
    }
    public void OnSelectMindBrain()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("At the midbrain, all trigeminal 2nd neuron fibers have already crossed sides, forming the trigeminothalamic tract, so the sensory loss would be to the contralateral side of the face as well.", Environment.NewLine), 52);

        gameObjectLabel13.SetActive(false);
    }
    public void OnSelectPons()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("The trigeminal nerve emerges at the pons with a large sensory root (and a smaller motor root). Lesion at this level would cause sensory loss pattern that includes proprioception and discriminative touch modalities at the ipsilateral side of face.", Environment.NewLine), 52);

        gameObjectLabel14.SetActive(false);
    }
    public void OnSelectNotInBrainstem()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        //Fail
        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("Fail. Please try again.", Environment.NewLine), 52);

        RegenerateRoom();
    }


    public void OnSelectMedialMedula()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectLabel17.SetActive(false);
        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("A lesion at this area causes the Medial Medullary syndrome, presenting with (among others): A loss of discriminative touch, conscious proprioception, and vibration sense on the contralateral side of the lesion(damage of medial lemniscus). Hemiparesis or hemiplegia on contralateral side of lesion(damage of pyramidal tract). Preservation of face sensation as the trigeminal nucleus is spared", Environment.NewLine), 52);
    }
    public void OnSelectEnd()
    {
        audioSourceAnswer.PlayOneShot(audioAnswer);

        gameObjectMedula.SetActive(false);
        gameObjectLabel17.SetActive(false);
        gameObjectLabel18.SetActive(false);

        gameObjectFilmViewMessage.GetComponent<TextMesh>().text = SpliceText(String.Format("Clinical case discussion {0}Loss of pain, temperature, touch and pressure on the ipsilateral side of the face is due to a lesion affecting the superficial aspect of the rostral medulla on that side. Descending primary neurons from the trigeminal nerves (amongst others) are damaged as they run in the spinal tract of trigeminal towards the spinal nucleus of trigeminal. If the lesion was deeper you would expect there to be total loss of sensation from the face as the trigeminothalamic tract runs medially within the brainstem.", Environment.NewLine), 52);

        stage = Stages.End.ToString();

        RegenerateRoom();
    }

    public void RegenerateRoom()
    {
        gameObjectLabel5.SetActive(false);
        gameObjectLabel6.SetActive(false);
        gameObjectLabel7.SetActive(false);
        gameObjectLabel8.SetActive(false);
        gameObjectLabel9.SetActive(false);
        gameObjectLabel10.SetActive(false);
        gameObjectLabel11.SetActive(false);
        gameObjectLabel12.SetActive(false);
        gameObjectLabel13.SetActive(false);
        gameObjectLabel14.SetActive(false);
        gameObjectLabel15.SetActive(false);
        gameObjectLabel16.SetActive(false);
        gameObjectLabel17.SetActive(false);
        gameObjectLabel18.SetActive(false);
        gameObjectMedula.SetActive(false);

        gameObjectBrainWithStelexos.SetActive(false);

        gameObjectLabel1.SetActive(true);
        gameObjectLabel2.SetActive(true);
        gameObjectLabel3.SetActive(true);
        gameObjectLabel4.SetActive(true);
        stage = Stages.Stage1.ToString();

    }

    public static string SpliceText(string text, int lineLength)
    {
        return Regex.Replace(text, "(.{" + lineLength + "})", "$1" + Environment.NewLine);
    }
}
