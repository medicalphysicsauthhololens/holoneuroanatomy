﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectBrain();
    }
}
