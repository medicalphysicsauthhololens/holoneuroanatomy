﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSelectMedusa : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectMedusa();
    }
}
