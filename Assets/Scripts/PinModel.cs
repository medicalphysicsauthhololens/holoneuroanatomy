﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinModel : MonoBehaviour {

    public float posX { get; set; }
    public float posY { get; set; }
    public float posZ { get; set; }

}
