﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainstemOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectBrainstem();
    }
}
