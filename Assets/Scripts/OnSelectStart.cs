﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSelectStart : MonoBehaviour {

	void OnSelect () {
        AnatomyManager.Instance.OnSelectStart();
	}
}
