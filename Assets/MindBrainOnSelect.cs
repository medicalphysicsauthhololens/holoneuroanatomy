﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MindBrainOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectMindBrain();
    }

}
