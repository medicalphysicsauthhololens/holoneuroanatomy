﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotInBrainOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectNotInBrain();
    }
}
