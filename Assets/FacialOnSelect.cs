﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacialOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectFacial();
    }
}
