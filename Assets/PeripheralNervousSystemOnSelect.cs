﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeripheralNervousSystemOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectPeripheralNervousSystem();
    }
}
