﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinalCordOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectSpinalCord();
    }
}
