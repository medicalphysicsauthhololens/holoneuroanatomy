﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VagusOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectVagus();
    }
}
