﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubCorticalWhiteOnSelect : MonoBehaviour {
    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectSubcorticalWhite();
    }
}
