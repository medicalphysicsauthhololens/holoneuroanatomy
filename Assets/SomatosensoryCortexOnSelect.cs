﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SomatosensoryCortexOnSelect : MonoBehaviour {
    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectSomatosensoryCortex();
    }
}
