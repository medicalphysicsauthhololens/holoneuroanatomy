﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotInBrainstemOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectNotInBrainstem();
    }
}
