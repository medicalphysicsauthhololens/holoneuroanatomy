﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternOfSensoryLossOnSelect : MonoBehaviour {

    void OnSelect()
    {
        AnatomyManager.Instance.OnSelectPatternOfSensoryLoss();
    }
}
